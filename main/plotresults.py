import pickle
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import string



def get_data(path):

    data = open(path,'rb')
    data=pickle.load(data)

    plotted_data= data.copy()
    for result_dic in plotted_data:
        subject_dict=plotted_data[result_dic]
        plotted_data[result_dic]={}
        for key in subject_dict:
            if isinstance(subject_dict[key],(float,int)):
                plotted_data[result_dic][key]=subject_dict[key]

    subjects_keys=list(plotted_data.keys())
    properties_keys= list(plotted_data[subjects_keys[0]].keys())

    data= tuple([plotted_data[key][key1] for key1 in properties_keys] for key in subjects_keys)
    data=[[[a,b,subjects_keys[i]] for (a,b) in zip(properties_keys,data[i])] for i in range(len(data))]

    return (subjects_keys,properties_keys,data)


def format_name(name):
    name1=name.split('-')
    return "subject{}".format(name1[0][-3:])



def plot_by_properties(path="results/res_analysis.pkl"):
    (subjects_keys,properties_keys,data)=get_data(path)
    print("Shows a plot for each of the {} properties".format(len(properties_keys)))
    for prop in properties_keys:
        dplot=[]
        dat=data.copy()
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        conditions = set()
        for elem in dat:
            for el in elem:

                if prop in el:
                    dplot.append(el[1:])
                    cond=el[2].split('-')[-1].split('_')[0]
                    conditions.add(cond)


            alph=string.ascii_lowercase

            i=0
            l1=[]
            l2=[]
            for cond in conditions:
                letter=alph[i]
                colors = cm.rainbow(np.linspace(0, 2, len(cond)))
                vars()[letter]= ax.scatter([format_name(a[1]) for a in dplot if (("fisher" in a[1]) and cond in a[1])],
                               [a[0] for a in dplot if (("fisher" in a[1]) and cond in a[1])], alpha=0.8, color=colors[i],
                               marker='^')
                l1.append(vars()[letter])
                l2.append("fisher {}".format(cond))
                letter = alph[i]
                vars()[letter] = ax.scatter([format_name(a[1]) for a in dplot if ((not "fisher" in a[1]) and cond in a[1])],
                               [a[0] for a in dplot if ((not "fisher" in a[1]) and cond in a[1])], alpha=0.8,
                               color=colors[i], marker='o')
                l1.append(vars()[letter])
                l2.append("normal {}".format(cond))
                i+=1

        plt.title(prop)
        plt.xticks(rotation=70, fontsize=8,ha='right' )
        plt.subplots_adjust(bottom=0.2, right=0.85, top=0.93)

        plt.legend(l1,l2,loc="lower left", bbox_to_anchor=(0.9,0.7))
        plt.show()






