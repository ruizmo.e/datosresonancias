import sys
from calculate_properties import *
from plotresults import *
import time


def calculate():
    start = time.time()
    path= 'No path given'
    try:

        result_name = sys.argv[3]
    except:
        arg = []
        result_name = 'res_analysis'

    try:
        path=sys.argv[2]
        files_list = dirs_list(path)
    except:
        raise Exception("Sorry, path \"{}\" not found".format(path))

    # Change thid parameter to chose desired threshold
    calculate_data_from_multiple_matrix(files_list, result_name,0.7)
    end = time.time()
    print("Time for all subjects: {}".format(end - start))
    plot_by_properties(os.path.join("results", result_name+'.pkl'))



def plot():
    try:
        path = sys.argv[2]
        plot_by_properties(path)

    except:
        try:
            plot_by_properties()
        except:
            raise Exception("No path has been given and default results file hasn't been found in your directory")


def main():
    option=None
    try:
        option = sys.argv[1]
    except:
        raise Exception("You must specify options: plot or calculate")
    if option != None:

        if option== 'plot':
            plot()

        elif option== 'calculate':
            calculate()

        else:
            raise Exception("You must select a correct option: plot or calculate")


    else:
        """Si no se pide calcular vamos directamente a la representación de datos"""
        plot()




if __name__ == '__main__':
    main()
