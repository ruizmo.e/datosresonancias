import numpy as np
import math
import os
from os import listdir
from os.path import isfile, isdir, join

"""File containing all auxiliar functions needed to calculate and plot the properties from the graph"""
def network(x):
    """parsing network's names"""
    x = x[0].replace(".", " ").split("networks ")[1]
    if "(L)" in x or "(R)" in x:
        a =  x.split("(")
        x = a[0] + " (" + a[1]
    else:
        x = x.split("(")[0]
    return x

def atlas(x):
    """parsing atlas names"""
    return x[0].split(".")[1]

def fishersInverse(Z):
    """Function to calculate fisher's inverse"""
    return ((math.exp(2*Z)-1)/(math.exp(2*Z)+1))
#Aplicate fisher's inverse for each element of a matrix
def inverseApplication(m):
    for i in range(len(m)):
        for j in range(len(m)):
            m[i][j] =fishersInverse(m[i][j])
    return m;

def converter(obj):
    """Function to format values to be saved in a results file """
    if isinstance(obj, np.integer):
        return int(obj)
    elif isinstance(obj, np.floating):
        return float(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()

def format_dir(dir):
    if '/' in dir:
        return dir.split('/')[-1].split('.')[-2]
    else: return dir.split('.')[-2]

def dirs_list(path):
    """receives a diretory and returns a list with path of all subjects data with extension.mat"""
    return [os.path.join(path, obj) for obj in listdir(path) if (isfile(os.path.join(path, obj)) and '.mat' in obj )]



def ls3(path):
    return [obj.name for obj in Path(path).iterdir() if obj.is_file()]